<?php

return [
    'services' => ['register','gateway','businessworker'],
    'registerPort'=> 1240,
    'gatewayWebsockertPort'=> 8284,
    'gatewayStartPort'=> 5000,
    'ssl'=>false,
    'context' => array(
            // 更多ssl选项请参考手册 http://php.net/manual/zh/context.ssl.php
            'ssl' => array(
                // 请使用绝对路径
                'local_cert'                 => '/root/dnmp/services/nginx/ssl/cert/colorui-im-h5.jc91715.top/colorui-im-h5.jc91715.top.cer', // 也可以是crt文件
                'local_pk'                   => '/root/dnmp/services/nginx/ssl/cert/colorui-im-h5.jc91715.top/colorui-im-h5.jc91715.top.key',
                'verify_peer'               => false,
                // 'allow_self_signed' => true, //如果是自签名证书需要开启此选项
            )
    ),
    'gatewaylanIp' => '127.0.0.1',//本机ip，分布式部署时使用内网ip
    'registerAddress' => '127.0.0.1:1240',

    
];
