<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'jcc/workerman',
  ),
  'versions' => 
  array (
    'jcc/workerman' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'workerman/gateway-worker' => 
    array (
      'pretty_version' => 'v3.0.20',
      'version' => '3.0.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '75b21653b3eaa99c4d5ae84ce8c5b27cda96d095',
    ),
    'workerman/gatewayclient' => 
    array (
      'pretty_version' => 'v3.0.13',
      'version' => '3.0.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f4e76f38947be5cabca2c6fee367151f248d949',
    ),
    'workerman/workerman' => 
    array (
      'pretty_version' => 'v4.0.19',
      'version' => '4.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af6025976fba817eeb4d5fbf8d0c1059a5819da3',
    ),
  ),
);
