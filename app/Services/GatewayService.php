<?php namespace App\Services;

use GatewayClient\Gateway;

class  GatewayService
{

    public function __construct()
    {
        // Gateway::$registerAddress = '172.17.0.1:1240';
        Gateway::$registerAddress = config('im.register_address');
    }

    
    public function bindUid($clientId,$userId)
    {
        //client_id/user_id
        Gateway::bindUid($clientId, $userId);

    }

    public function joinGroup($clientId,$groupId)
    {

        //client_id/group_id
        Gateway::joinGroup($clientId, $groupId);

    }

    public function sendToUid($userId,$message,$event=null)
    {
        if($event){//适配事件模块
            $data = [
                'cmd' => 'upgame',
                'data' => [
                    'cmd' => $event,
                    'data' => $message,
                ],
            ];
            $message = $data;
        }
        //user_id/[]
        Gateway::sendToUid($userId, json_encode($message));

    }

    public function sendToGroup($groupId,$message,$clientId = null,$event = null)
    {

        if($event){//适配事件模块
            $data = [
                'cmd' => 'upgame',
                'data' => [
                    'cmd' => $event,
                    'data' => $message,
                ],
            ];
            $message = $data;
        }
        \Log::info('groupId:'.$groupId, $message);
        \Log::info('registerAddress:'.Gateway::$registerAddress);
        //group_id/[]/user_id(当前用户不推送)
        Gateway::sendToGroup($groupId, json_encode($message),$clientId);
    }

    public function getClientIdByUid($userId)
    {
        return Gateway::getClientIdByUid($userId);
    }

    public function isUidOnline($userId)
    {
        //user_id
        return Gateway::isUidOnline($userId);
    }

}