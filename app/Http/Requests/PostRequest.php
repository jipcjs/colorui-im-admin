<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $method = $this->route()->getActionMethod();
        switch ($method) {
            case 'index':
                $rules = [
                    'type' => ['required','in:pull,up'],
                    'unique_slug' => [],
                ];
                break;
            case 'publish':
                $rules = [
                    'post_messages' => ['required','array'],
                    'post_messages.*.type' => ['required','in:friend,group'],
                    'post_messages.*.message_type'=> ['required','in:text,image,audio,file,video,two-click-avatar,two-click-text'],
                    'post_messages.*.to' => ['required'],
                    'post_messages.*.data' => ['required'],
                    'post_messages.*.unique_slug'=>'required'
                ];
                break;
            case 'joinGroup':
                $rules = [
                    'group_id' => ['required'],
                ];
                break;
        }
        return $rules;
    }
}
