<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organization;
use App\Models\User;

class ThirdPartyController extends Controller
{
    //第三方传入唯一标识符 来生成系统用户token，第三方拿到后,和自己用户进行关联
    public function getUserToken(Request $request)
    {
        $request->validate([
            'unique_identifier' =>'required|string',
            'app_id' =>'required|string',
            'name' => 'required|string',
            'tag' => 'required|in:0,1,2',//0正常用户，1顾客，2客服 
            'avatar' => 'required|string',
            'extra' => [],
            'sign' => 'required'//签名
        ]);

        $this->verificationSign();

        $user = User::where('unique_identifier', $request->unique_identifier)->first();
        $new = false;
        if (!$user) {
            $user = new User();
            $user->unique_identifier = $request->unique_identifier;
            $new = true;
        }
        $user->name = $request->name;
        $user->tag = $request->tag;
        $user->avatar = $request->avatar;

        if ($request->extra) {
            $user->extra = $request->extra;
        }

        $user->save();

        // if ($new) {
        event(new \App\Events\ThirdParty\CreateUser($user));
        // }
        return response()->json(['code'=>0,'msg'=>'ok','data'=>[
            'token'=>$user->createToken('third-party')->plainTextToken],
            'new'=>$new
            ]);
    }



    public function getUserTokens(Request $request)
    {
        $request->validate([
            'unique_identifier' =>'required|string',
            'app_id' =>'required|string',
            'sign' => 'required'
        ]);
        $this->verificationSign();


        $user = User::where('unique_identifier', $request->unique_identifier)->first();

        if (!$user) {
            return response()->json(['code'=>1,'msg'=>'不存在的用户','data'=>[]]);
        }

        return response()->json(['code'=>0,'msg'=>'','data'=>['tokens'=>$user->tokens]]);
    }

    public function deleteUserToken(Request $request)
    {
        $request->validate([
            'unique_identifier' =>'required|string',
            'app_id' =>'required|string',
            'token_id' =>'required',
            'sign' => 'required'
        ]);
        $this->verificationSign();

        $user = User::where('unique_identifier', $request->unique_identifier)->first();
        if (!$user) {
            return response()->json(['code'=>1,'msg'=>'不存在的用户','data'=>[]]);
        }

        $token = $user->tokens()->where('id', $request->token_id)->first();

        if (!$token) {
            return response()->json(['code'=>1,'msg'=>'不存在的token id','data'=>[]]);
        }
        $token->delete();
        return response()->json(['code'=>0,'msg'=>'删除成功','data'=>[]]);
    }

    protected function verificationSign()
    {
        $sign = request()->sign;
        $data = request()->all();
        // dd($data);
        unset($data['sign']);
        // $org = Organization::findOrFail($data['app_id']);
        $org = Organization::where('app_id', $data['app_id'])->firstOrFail();

        $secret = $org->app_secret;
        $vSign = getSignature($data, $secret);
        if ($sign!=$vSign) {
            throw new \App\Exceptions\RuntimeException('签名不正确');
            return response()->json(['code'=>1,'msg'=>'签名不正确','data'=>[]]);
        }
        // $d= [
        //     "unique_identifier" => "123456",
        //     "name" => "sdasd",
        //     "app_id" => "im202106122159061836914460",
        //     "avatar" => "field",
        // ];
    }
}
