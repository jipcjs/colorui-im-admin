<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\ImSend' => [
            'App\Listeners\SaveMessage',
        ],
        'App\Events\RandomChatJoining' => [//加入兴趣小组
            'App\Listeners\NotificationGroupRandomChatUser',
        ],
        'App\Events\RandomChatApply' => [//兴趣小组满了之后，申请加入兴趣小组，通知兴趣小组的成员，需要兴趣小组成员的一半同意才可以加入
            'App\Listeners\NotificationGroupRandomChatUserJoined',
        ],
        'App\Events\MessageReplace' => [//消息替换
            'App\Listeners\NotificationMessageReplace',
        ],
        'App\Events\MessageActionVote' => [//消息点赞 (分为消息点赞，和文章点赞，文章也是一条消息)
            'App\Listeners\NotificationMessageActionVote',
        ],
        'App\Events\RefreshUser' => [//刷新用户
            'App\Listeners\NotificationRefreshUser',
        ],
        'App\Events\ReplaceOrInsertLatestMessage' => [//更新最新消息
            'App\Listeners\NotificationReplaceOrInsertLatestMessage',
        ],
        'App\Events\ReplaceOrInsertPostLatestMessage' => [//更新朋友圈的最新消息(回复数量)
            'App\Listeners\NotificationReplaceOrInsertPostLatestMessage',
        ],
        'App\Events\ThirdParty\CreateUser' => [//第三方创建一个普通用户
            'App\Listeners\ThirdParty\TriggerCreateUser',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
