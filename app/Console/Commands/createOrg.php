<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Organization;

class createOrg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:org {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');

        if(!$name){
            $this->info('缺少参数');
        }

        $org = Organization::createOrganization([
            'name' => $name
        ]);

        $this->info('创建成功');
        $this->info("app_id:\n".$org->app_id);
        $this->info("app_secret:\n".$org->app_secret);

        return 0;
    }
}
