<?php

namespace App\Listeners;

use App\Events\RefreshUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotificationRefreshUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RefreshUser  $event
     * @return void
     */
    public function handle(RefreshUser $event)
    {
        $groupId = $event->groupId;//自己的群聊id，相当于通知自己。
        app('gateway')->sendToGroup($groupId,['type'=>'refresh_user'],null,'im_refresh_user');//刷新用户信息
    }
}
