<?php

namespace App\Listeners\ThirdParty;

use App\Events\ThirdParty\CreateUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\User;

class TriggerCreateUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateUser  $event
     * @return void
     */
    public function handle(CreateUser $event)
    {
        $user = $event->user;
        $user->createSelfGroup();
        if ($user->tag==1) {
            $customerGroup = $user->createCustomerGroup();
            $customerGroup->users()->syncWithoutDetaching(User::where('tag', 2)->get('id')->pluck('id')->toArray());
            
            //告诉客服，有新客户加入进来了,可放到事件中todo
            $gateway = app('gateway');
            $users = $customerGroup->users()->with(['tokens'=>function ($query) {
                $query->where('last_used_at', '>', now()->subMinutes(60*24));//一天之内的才推送
            }])->get();
            foreach ($users as $user) {
                foreach ($user->tokens as $token) {
                    // if($currentAccessToken->id!=$token->id){//都需要重新绑定
                    $gateway->sendToUid($token->id, ['type'=>'refresh_user'], 'im_refresh_user');
                    // }
                }
            }
        }
    }
}
