<?php

namespace App\Listeners;

use App\Events\MessageReplace;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Http\Resources\MessageResource;

class NotificationMessageReplace
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageReplace  $event
     * @return void
     */
    public function handle(MessageReplace $event)
    {
        $groupId = $event->groupId;
        $message = $event->message;


        $sendData = [
            'type'=>'im_message_replace',
            'content' => new MessageResource($message)
        ];
        app('gateway')->sendToGroup($groupId,$sendData,null,'im_message_replace');
    }
}
