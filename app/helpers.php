<?php

function getSignature($params, $secret)
{
    // $secret = config('app.secret_key');


    //先将参数以其参数名的字典序升序进行排序
    ksort($params);
    //遍历排序后的参数数组中的每一个key/value对
    $string = "";
    foreach ($params as $key => $value) {
        if(is_array($value)){
            $value = json_encode(ksort($value));
        }
        if ($string) {
//                $string .= '&'. $value;
            $string .= '&' . $key . "=" . $value;
        } else {
//                $string .=  $value;
            $string .= $key . "=" . $value;
        }
    }
    //将签名密钥拼接到签名字符串最后面
    $string .= $secret;
    //通过md5算法为签名字符串生成一个md5签名，该签名就是我们要追加的sign参数值
    return md5($string);
}
