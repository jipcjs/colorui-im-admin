<?php

namespace App\Exceptions;

use Exception;

class TestException extends Exception
{
    public function render($request)
    {
        return response()->json(['code'=>1,'msg'=>$this->getMessage(),'data'=>json_decode($this->getMessage(),)]);
    }
}
