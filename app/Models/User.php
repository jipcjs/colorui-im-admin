<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,HasFactory, Notifiable;




    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'extra' => 'json'
    ];


    public function divideGroups()
    {
        return $this->hasMany(DivideGroup::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class,'group_users','user_id','group_id')->where('type',1);

    }
    public function createGroups()
    {
        return $this->hasMany(Group::class);

    }

    //好友小群 只有两人
    public function friendGroups()
    {
        return $this->belongsToMany(Group::class,'group_users','user_id','group_id')->where('type',0);
    }

    public function getFriendUserIds()
    {
        $divideGroups = $this->divideGroups()->with('users')->get();

        $friendUserIds = [$this->id];
        foreach ($divideGroups as $divideGroup){
            $friendUserIds = array_merge($friendUserIds,$divideGroup->users->pluck('id')->toArray());
        }

        return $friendUserIds;
    }

    public function getFriendGroupIds()
    {
        $friendUserIds = $this->getFriendUserIds();

        $userToUsers = [];
        foreach ($friendUserIds as $friendUserId){
            $userToUsers[] = $friendUserId.'-'.$friendUserId;
        }
        $friendGroups = Group::whereIn('user_to_user', $userToUsers)->get();

        return $friendGroups->pluck('id')->toArray();
    }

    public function getGroupIds()
    {
        return $this->groups()->get()->pluck('id')->toArray();
    }

    public function getSelfGroup()
    {
        return Group::where('user_to_user', $this->id.'-'.$this->id)->first();
    }

    public function getSelfGroupByFriendGroups()
    {
        return $this->friendGroups->where('user_to_user', $this->id.'-'.$this->id)->first();
    }
    public function getSelfCustomerGroupByGroups()
    {
        return $this->groups->where('user_id', $this->id)->where('type', 1)->where('extra_type',3)->first();
    }

    public function createSelfGroup()
    {
        $a =[];
        $a[]=$this->id;
        $a[]=$this->id;
        sort($a);
        $user_to_user = implode('-',$a);
        $group = $this->getSelfGroup();
        if(!$group){
            $group = new \App\Models\Group();
            $group->user_to_user = $user_to_user;
            $group->type = 0;
            $group->name = $this->name;
            $group->avatar = $this->avatar;
            $group->save();
            $group->users()->sync($a);
        }
    }

    public function createCustomerGroup()
    {
        $customerGroup = Group::where('user_id', $this->id)->where('type', 1)->where('extra_type',3)->first();
        if(!$customerGroup){
            $customerGroup = \App\Models\Group::factory()->create();
            $customerGroup->name = $this->name.'的联系群';
            $customerGroup->user_id = $this->id;
            $customerGroup->type = 1;
            $customerGroup->extra_type = 3;
            $customerGroup->save();
            $customerGroup->users()->syncWithoutDetaching([$this->id]);

        }
        return $customerGroup;
    }
   
}
